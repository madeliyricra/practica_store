import React from "react"
import { Body, Container, ContainerImage, Description, Image, Title } from "./styled"

export interface IProduct {
  id: string,
  image: string,
  title: string,
  description?: string,
  onclick?: (id: string) => void,
  onMouseLeave?: React.MouseEventHandler<HTMLDivElement>,
  onMouseOver?: React.MouseEventHandler<HTMLDivElement>
}

const defaultProps = {
  // title: 'Producto 1',
  // image: 'https://http2.mlstatic.com/D_NQ_NP_657258-MLA47664529266_092021-W.jpg',
}

/**
 * 
 * @param props.id string
 * @param props.image string
 * @param props.title string
 * @param props.description? string
 * @param props.onclick? (id: string) => void
 * @param props.onMouseLeave? React.MouseEventHandler<HTMLDivElement>
 * @param props.onMouseOver? React.MouseEventHandler<HTMLDivElement>
 * 
 * @returns JSX.Element
 */

const Product = (props: IProduct): JSX.Element => {
  props = {...defaultProps, ...props}
  const { image, title, description, onclick, onMouseLeave, onMouseOver } = props

  const handleClick = () => {
    if(typeof onclick === 'function') onclick
  }

  return (
    <Container onClick={handleClick} onMouseLeave={onMouseLeave} onMouseOver={onMouseOver} >
      <ContainerImage>
        <Image src={image} />
      </ContainerImage>
      <Body>
        <Title>{title}</Title>
        <Description>{description}</Description>
      </Body>
    </Container>
  )
}

export default Product