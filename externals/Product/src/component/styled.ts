import styled from "styled-components";

export const Container = styled.div`
  width: 180px;
  background-color: #1c1719;
  background-color: #1c1719;
  color: #fff;
  border-radius: 10px;
  overflow: hidden;
`

export const ContainerImage = styled.div`
  width: 100%;
  background-color: #fff;
`

export const Image = styled.img`
  width: 100%;
  height: 150px;
  object-fit: contain;
`

export const Body = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 10px;
`

export const Title = styled.h4`
  margin: 0;
`

export const Description = styled.p`
  font-size: 15px;
`