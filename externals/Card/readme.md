# [Template](http://casesandberg.github.io/react-color/)

## Installation & Usage

```sh
npm install @react/card
```

### Include the Component

```js
import Card from '@react/card'

<Card {...item} />
```