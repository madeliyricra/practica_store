import React from 'react'
import { Container } from './styled'

export interface IGrid {
  children: React.ReactNode,
  style?: React.CSSProperties
}

const defaultProps = {
  style: {}
}

const Grid = (props: IGrid) => {
  props = {...defaultProps, ...props}
  const { children } = props
  return (
    <Container>
      {children}
    </Container>
  )
}

export default Grid