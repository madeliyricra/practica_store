import Product from '@react/product'
import Grid from '@react/grid'
import { useState } from 'react'
import { ICard } from './components/Card'
import Card from '@react/card'

const App = (): JSX.Element => {
  const [stores, setStores] = useState<ICard[]>([
    {
      id: '1',
      title: 'Tienda 1',
      description: 'Lorem Ipsum is Lorem Ipsum, Lorem Ipsum',
      image: '/vite.svg'
    },
    {
      id: '2',
      title: 'Tienda 2',
      description: 'Lorem Ipsum is Lorem Ipsum, Lorem Ipsum',
      image: '/vite.svg'
    },
    {
      id: '3',
      title: 'Tienda 3',
      description: 'Lorem Ipsum is Lorem Ipsum, Lorem Ipsum',
      image: '/vite.svg'
    },
    {
      id: '4',
      title: 'Tienda 4',
      description: 'Lorem Ipsum is Lorem Ipsum, Lorem Ipsum',
      image: '/vite.svg'
    }
  ])

  const handleSelect: ICard['onclick'] = (id) => {
    console.log(id)
  }

  return (
    <div>
      <Grid>
        <Product 
          id='1' 
          title='Telefono' 
          image={'https://http2.mlstatic.com/D_NQ_NP_657258-MLA47664529266_092021-W.jpg'} 
          onMouseLeave={() => console.log('onMouseLeave')}
          onMouseOver={() => console.log('onMouseOver')}
        />
        <Product id='2' title='Telefono' image={'https://http2.mlstatic.com/D_NQ_NP_657258-MLA47664529266_092021-W.jpg'} />
        {
          stores?.map((store, key) => <Card key={key} {...store} onclick={handleSelect} />)
        }
      </Grid>
    </div>
  )
}

export default App
