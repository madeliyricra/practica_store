import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  gap: 10px;
  padding: 30px 15px;
  width: 180px;
  background-color: #1c1719;
  color: #fff;
  border-radius: 10px;
  cursor: pointer;
  &:hover{
    // background: #e9e9e9;
  }
`

export const Title = styled.h5`
  margin: 0;
  font-size: 15px;
`

export const Description = styled.p`
  margin: 0;
  text-align: center;
  font-size: 12px;
`