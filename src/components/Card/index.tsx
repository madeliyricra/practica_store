import Avatar from "../Avatar"
import { Container, Description, Title } from "./styled"

export interface ICard {
  id: string,
  title: string,
  description?: string,
  image: string,
  onclick?: (id: string) => void
}

/**
 * 
 * @param props.id Identificador del elemento 
 * @param props.title Titutlo del elemento 
 * @param props.description Descripcion del elemento 
 * @param props.image Imagen del elemento 
 * @param props.onclick Callback que retorna un identificador
 * 
 * @returns JSX.Element
 */

const Card = (props: ICard): JSX.Element => {
  const { id, title, description, image, onclick } = props

  const handleClick = () => {
    if(typeof onclick === "function") onclick(id)
  }

  return (
    <Container onClick={handleClick}>
      <Avatar image={image} />
      <Title>{title}</Title>
      <Description>{description}</Description>
    </Container>
  )
}

export default Card