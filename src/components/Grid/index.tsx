import styled from "styled-components"

const Grid= styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-evenly;
  gap: 15px;
  align-self: center;
  alig-content: center;
  width: 100%;
  height: 100%;
  overflow: auto;
` 

export default Grid