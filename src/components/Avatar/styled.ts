import styled from "styled-components";

export const Container = styled.div`
  padding: 2px;  
  display: grid;
  place-items: center;
  border-radius: 50%;
  border: 2px solid #fff;
`

export const Image = styled.img`
  width: 70px;
  height: 70px;
  object-fit: scale-down;
  background-color: #291770;
  border-radius: 50%;
`