import { useState } from "react"
import { Container, Image } from "./styled"

interface IAvatar{
  image: string
}

const Avatar = (props: IAvatar): JSX.Element => {
  const { image } = props
  const [visible, setVisible] = useState<boolean>(false)

  const handleClick = () => {
    setVisible(!visible)
  }

  return (
    <Container onClick={handleClick}>
      <Image src={image}/>
    </Container>
  )
}

export default Avatar